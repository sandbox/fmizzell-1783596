<?php

class RelationManager{
  
  protected $relation_type;
  protected $parent;
  protected $new_children;
  protected $children;
  
  /**
   * A class that makes it easier to deal with relations
   * @param $relation
   * it could be a string, the name of the relation,
   * of the relation entity itself 
   */
  
  public function __construct($relation_type, $entity_type, $parent){
   
    $this->relation_type = $relation_type;
    $this->parent = entity_metadata_wrapper($entity_type, $parent);
    $this->children = array();
    $this->new_children = array();
  }
  
  public function addChild($entity_type, $entity){
      $this->new_children[] = entity_metadata_wrapper($entity_type, $entity);
  }
  
  /**
   *
   * @param array $entities an array of entity api entities
   */
  public function addChildren($entity_type, $entities){
    foreach($entities as $entity){
      $this->addChild($entity_type, $entity);
    }
  }
  
  
  public function getChildrenIds($children_type, $query = NULL){
    
    $pentity_type = $this->parent->type();
    
    $pid = $this->parent->getIdentifier();
    
    $rquery = relation_query($pentity_type, $pid);    
    $rquery->fieldCondition('endpoints', 'entity_type', $children_type, '=');


    $data = $rquery->execute();
    $relations = array();
    if(!empty($data)){
      $relations = entity_load('relation', array_keys($data));
    }

    $ids = array();
    foreach($relations as $rid => $relation){
      foreach(array(0,1) as $endpoint){
        $type = $relation->endpoints[LANGUAGE_NONE][$endpoint]['entity_type'];
        //if this is not the parent
        if(substr_count($type, $pentity_type) <= 0){
          $ids[] = $relation->endpoints[LANGUAGE_NONE][$endpoint]['entity_id'];
        }
      }
    }
    
    //if we got a query for further filtering, lets do that
    if($query){
      $query->entityCondition('entity_type', $children_type, '=');
      $query->propertyCondition('id', $ids, "IN");
      $data = $query->execute();
      if(array_key_exists($children_type, $data)){
        $ids = array_keys($data[$children_type]);
      }else{
        $ids = array();
      }
    }
    
    return $ids;
  }
  
  /**
   *
   * @param EntityFieldQuery $query A query object with all the desired condition 
   *  by which we want to filter the children ex. bundle, propertyConditions, etc
   * @return array An array with the children entities 
   */
  public function getChildren($children_type = NULL, $query = NULL){
    $ids = $this->getChildrenIds($children_type, $query);
    $entities = entity_load($children_type, $ids);
    return $entities;  
  }
  
  public function showChildren(){
    dpm($this->new_children);
  }
  
  /**
   * Create Relations of the given type between
   * the parents and the children
   */
  public function createRelations(){
    foreach($this->new_children as $child){
      $this->createRelation($child);
    }
  }
  
  private function createRelation($child){
    $endpoints = array();
    $index = 0;
    foreach(array($this->parent, $child) as $object){
      $endpoints[$index] = array(
        'entity_type' => $object->type(),
        'entity_id' => $object->getIdentifier(),
        'entity_bundle' => $object->getBundle(),
        'r_index' => $index
      );
      $index++;
    }
    
    relation_save(relation_create($this->relation_type, $endpoints));
  }
}
